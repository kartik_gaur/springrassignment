<?php
namespace App\Repositories;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class BaseRepository{
/**
     * @var Model
     */
    private $model;
    private $queryBuilder;

    public function __construct(Model $model)
    {
        $this->setModel($model);
        $this->setQueryBuilder($this->getModel());
    }

    protected function setModel(Model $model)
    {
        $this->model = $model;

        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setQueryBuilder($queryBuilder)
    {
        return $this->queryBuilder = $queryBuilder;
    }

    public function fetchOne(array $conditions = [])
    {
        $result = $this->getQueryBuilder()->where($conditions)->first();
        $this->resetQueryBuilder();

        return $result;
    }

    public function setQueryBuilder($queryBuilder)
    {
        return $this->queryBuilder = $queryBuilder;
    }
    
    private function resetQueryBuilder()
    {
        $this->setQueryBuilder($this->getModel());
    }
}