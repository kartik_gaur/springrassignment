<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    //table name
    protected $table = 'user_infos';

    //primary key
    protected $primaryKey = 'id';
}
