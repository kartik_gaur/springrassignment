<?php

namespace App\Http\Controllers;

use App\UserInfo;
use Illuminate\Http\Request;

class UserInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {   
        $users = UserInfo::where('is_active',1)->orderBy('DOJ')->get();
        
        return view('user_details')->with('users',$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $request->validate([
        'name' => 'required|max:255',
        'email' => 'required|email|max:255',
        'doj' => 'date|before_or_equal:today',
        'dol' => 'date|before_or_equal:today',
        'avatar' => 'image|max:1999',
        ]);
    
        $data = $this->sanitizeData($request);

        UserInfo::create($data);

        return redirect('/user')->with('success', 'User Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = UserInfo::findOrFail($id);
        $user->delete();

        return redirect('/user')->with('success','User Removed Succefully');
    }

    //sanitize form data
    private function sanitizeData($request){
        $data = [];
        $data['name'] = $request->name ?? '';
        $data['email'] = $request->email ?? '';
        $data['DOJ'] = $request->doj ?? date('d/m/Y');
        $data['DOL'] = $request->dol ?? '';
        if($request->hasFile('avatar')){
            $fileNameWithExt = $request->file('avatar')->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $fileToStore = $fileName.'_'.time().'.'.$extension ;
            $path = $request->file('avatar')->storeAs('public/avatar',$fileToStore);
        }
        else{
            $fileToStore = 'noimage.jpg';
        }
        $data['image'] = $fileToStore ;
        return $data ;
    }
}
