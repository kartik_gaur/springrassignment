<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('js/app.js') }}">
    <link rel="stylesheet" href="{{ asset('js/bootstrap.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Users</title>
</head>
<body>
    <div class="container">
      @include('includes.messages')
        <div class="row">
            <div class="col-md-8">User Records</div>
            <div class="col-md-4">
                <button class="btn btn-primary" data-toggle="modal" data-target="#addMemberModal">Add New</button>
            </div>
        </div>
        <div id="addMemberModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Add Member</h4>
                </div>
                <div class="modal-body">
                  <form action="/user" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" placeholder="name@example.com">
                      </div>
                      <div class="form-group">
                        <label for="name">Full Name</label>
                        <input type="text" name="name" id="name" placeholder="John Doe">
                      </div>
                      <div class="form-group">
                        <label for="doj">Date Of Joining</label>
                        <input type="date" name="doj" id="doj">
                      </div>
                      <div class="form-group">
                        <label for="doj">Date Of Leaving</label>
                        <input type="date" name="dol" id="dol">
                      </div>
                      <div class="form-group">
                        <label for="avatar">Upload image</label>
                        <input type="file" name="avatar" id="avatar">
                      </div>
                      <div class="fomr-group">
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default">Save</button>
                        </div>
                      </div>
                  </form>
                </div>
              </div>
          
            </div>
          </div>
        <table class="table table-striped table-bordered usersTable">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Avatar</th>
                    <th scope="col">Name</th>
                    <th scope="col">Eamil</th>
                    <th scope="col">Experience</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($users) && $users->isNotEmpty())
                @foreach ($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td><img src="storage/avatar/{{$user->image}}" style = "vertical-align: middle;width: 50px;height: 50px;border-radius: 50%;"alt="" srcset=""></td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        @php
                            $to = empty($user->DOL) ? date('d-m-Y') : $user->DOL;
                            $diff = date_diff(date_create($user->DOJ),date_create($to));
                            
                            $experience = '';
                            if(!empty($diff->y))
                            $experience = $diff->y.'Years ';
                            if(!empty($diff->m))
                            $experience = $experience .$diff->m.'Months';
                        @endphp
                        {{$experience}}
                    </td>
                    <td><button type="button" id="btnRowClick" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#removeMemberModal" >Remove</button></td>
                </tr>
                @endforeach
                    
                @endif
                
            </tbody>
        </table>
        <div id="removeMemberModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
          
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Are you sure you wan't to delete?</h4>
                </div>
                <div class="modal-body">
                    <form id="deleteUser" action="" method="post">
                        {{method_field("DELETE")}}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <button type="submit" class="btn btn-danger">Yes</button>
                            <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                        </div>
                    </form>
              </div>
    </div>
    
</body>
<script>
    $(".usersTable").on('click','#btnRowClick',function(){
        var currentRow=$(this).closest("tr"); 
        var id = currentRow.find("td:eq(0)").text();
        $("#deleteUser").attr('action',`/user/${id}`);
    });
</script>
</html>